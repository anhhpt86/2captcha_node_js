**Install**

cd 2Captcha_Node_Js

npm update

---

## Usage

```javascript
const { solveCaptchaV2 } = require('./captchaapi');
//default value
const siteDetails = {
    sitekey: '6LccSjEUAAAAAN-WiRxCZ5CzsjR_123456', // key site Captcha
    pageurl: 'https://captcha-delivery.com', // url destination 
};
const api = {
    in: 'https://2captcha.com/in.php',
    res: 'https://2captcha.com/res.php',
    key: '123456789', // account key at 2captcha.com
    pollingInterval: 5000
};
const params = {
    key: api.key,
    method: 'userrecaptcha',
    googlekey: siteDetails.sitekey,
    pageurl: siteDetails.pageurl,
    json: 1,
    version:'v2', // v3, Enterprise
    header_acao:1,

    // proxy: 'username@password.com',
    // proxyType:'HTTP',
    // min_score:0.3, // using with google captcha v3
    // invisible:1, // using with google captcha v2 invisible
    // action:'globalRecaptchaCallback', // callbackk function javascript
};
(async () => {

    // solve the captcha with API
    console.log("* Waiting g-recaptcha-reponse");

    const token = await solveCaptchaV2(params, api);
    console.log('* Receive g-recaptcha-reponse: ' + token);
    
    // unsccessful case 
    if (token.includes('ERROR') || token.includes('CAPCHA_NOT_READY')){
        process.exit(1)
    }
   
  })();
```

---

## Motivation

These days captchas are unfortunately everywhere, with reCAPTCHA having the biggest "market share" in that space (> 80%). The situation got really bad, with privacy minded users (tracking blocker, VPNs) being penalized heavily and having to solve a lot of reCAPTCHA challenges constantly while browsing the web.

These small codes allow us to get automatically google token from 2Captcha Service and bypass google's protection. It is very important for crawler's activites.

---